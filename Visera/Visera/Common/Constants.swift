//
//  Constants.swift
//  Visera
//
//  Created by Alex on 5/5/17.
//  Copyright © 2017 __DevInteractive__. All rights reserved.
//

import Foundation

//Global size constant
let kScreenSize = UIScreen.main.bounds
let kScreenWidth = kScreenSize.width
let kScreenHeight = kScreenSize.height


// константы для получения действия от tapGesture
let kDeltaVelosity:CGFloat = 100
let kConstantForPush:CGFloat = 1000

//время анимации для позиции верхней шторки
let kConstantForAnimateing:Double = 0.5
let kConstantForSetPosition:Double = 0.5


// время анимации при клике на ячейку пользователя
let kConstantSelectContact:Double = 0.0


// цвет и высота нижней линии на шторке
let backImageAvatarBottomLineWidth:CGFloat = 25
let backImageAvatarBottomLineColor = UIColor.black.withAlphaComponent(0.5).cgColor

//cells in collection view
let kAvatarBorderLineWidth:CGFloat = 2
let kAvatarBorderLineColor = UIColor.white.cgColor

//avatar backImage 
//first (нижний аватар под контроллерами)
let kAvatarBackAlpha:CGFloat = 0.7
let kAvatarBackBlurAlphaFirst:CGFloat = 0.8

//second (верхний аватар под контактами)

let kTopAvatarBackColor:UIColor = UIColor.white
let kTopAvatarBackColorOpacity:CGFloat = 0.25
let kAvatarBackBlurAlpha:CGFloat = 0.8

//avatarView (current and previous avatar in top slide)
let kAvatarLineColor:CGColor = UIColor.white.cgColor
let kAvatarLineWidth:CGFloat = 2
let kCurrentAvatarSize:CGFloat = 100
let kPreviousAvatarSize:CGFloat = 80
let kCurrentAvatarRadius:CGFloat = kCurrentAvatarSize / 2
let kPreviousAvatarRadius:CGFloat = kPreviousAvatarSize / 2


//button view
//ButtonPanel
let kYOffset:CGFloat = 180
let kYOffsetBottom:CGFloat = 80
let kYOffsetForInitialAvatarView:CGFloat = 50

let kScaleButtonSize:CGSize = CGSize(width: 50, height: 50)
let kScaleButtonLeftOffset: CGFloat = 30

let kCreateButtonSize:CGSize = CGSize(width: 80, height: 80)
let kCreateButtonRightOffset: CGFloat = 20
let kCreateButtonLeftOffset: CGFloat = 115

