//
//  ShadowImage.swift
//  Visera
//
//  Created by Ilia Gutu on 4/24/17.
//  Copyright © 2017 __DevInteractive__. All rights reserved.
//

import UIKit

class ShadowImageView: UIImageView {
    
    override func layoutSubviews() {
        super.layoutSubviews()
   
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = bounds
        gradient.colors = [UIColor.black.cgColor, UIColor.clear.cgColor]
        gradient.locations = [0.0, 1.0]
        layer.insertSublayer(gradient, at: 0)
   
    }
}
