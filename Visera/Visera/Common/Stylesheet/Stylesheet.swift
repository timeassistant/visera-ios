//
//  Stylesheet.swift
//  Visera
//
//  Created by Ilia Gutu on 4/19/17.
//  Copyright © 2017 __DevInteractive__. All rights reserved.
//

import UIKit


enum ViseraColor {

    case orange
    case green
    case lightGreen
    case red
    case lightBlue
    case black
    case ultralightGray
    case lightGray
    case gray
    case darkGray

    var color: UIColor {
        switch self {
        case .orange: return UIColor.with(hex:"#F47E20")
        case .green: return UIColor.with(hex:"#00A44F")
        case .lightGreen: return UIColor.with(hex:"#69BC45")
        case .red: return UIColor.with(hex:"#BC222F")
        case .lightBlue: return UIColor.with(hex:"#55C8EC")
        case .black: return UIColor.with(hex:"#000000")
        case .ultralightGray: return UIColor.with(hex:"#989898")
        case .lightGray: return UIColor.with(hex:"#5C5C5C")
        case .gray: return UIColor.with(hex:"#4D4D4D")
        case .darkGray: return UIColor.with(hex:"#141618")
        }
    }
}

enum ViseraFont: String {

    case regular = "MyriadPro-Regular"
    case light = "MyriadPro-Light"
    case bold = "MyriadPro-Bold"

    func font(size: CGFloat) -> UIFont {
        return UIFont(name: rawValue, size: size)!
    }
}
