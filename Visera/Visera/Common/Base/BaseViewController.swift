//
//  BaseViewController.swift
//  Visera
//
//  Created by Ilia Gutu on 4/20/17.
//  Copyright © 2017 __DevInteractive__. All rights reserved.
//

import UIKit
import PureLayout

class BaseViewController: UIViewController {

    var base: BasePagingViewController!
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addMotion(within: 10)
        setup()
        // Do any additional setup after loading the view.
    }
    
    func setup(){
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
