//
//  BasePagingViewController.swift
//  Visera
//
//  Created by Ilia Gutu on 4/19/17.
//  Copyright © 2017 __DevInteractive__. All rights reserved.
//

import UIKit
import TinyConstraints
import PureLayout

class BasePagingViewController: UIViewController {

    var scrollView = UIScrollView()
    var viewControllers = [UIViewController]()
    
    var background: UIImageView!
    
    var image: UIImage?{
        didSet{
            self.background.image = image
        }
    }
    
    convenience init(viewControllers:[UIViewController])
    {
        self.init()
        self.viewControllers = viewControllers
        
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupImageBackground()
        initViews()
    
        setupParralax()
      
        // Do any additional setup after loading the view.
    }

    func setupImageBackground(){
        background = UIImageView()
        background.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(background)
        background.autoPinEdgesToSuperviewEdges()
        image = UIImage(named: "background")
        
    }
    
    func setupParralax(){

        let bounds = UIScreen.main.bounds
        let width = bounds.size.width
        let height = bounds.size.height;
        
        let count = CGFloat(self.viewControllers.count)
        scrollView.contentSize = CGSize(width: count*width, height: height);
        scrollView.isPagingEnabled = true

        
        var idx:Int = 0;
        
        
        for viewController in viewControllers {
            // index is the index within the array
            // participant is the real object contained in the array
            addChildViewController(viewController);
            let originX:CGFloat = CGFloat(idx) * width;
            viewController.view.frame = CGRect(x: originX, y: 0, width: width, height: height);
            scrollView.addSubview(viewController.view)
            viewController.didMove(toParentViewController: self)
            idx += 1;
        }
    }
    
    func initViews(){
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(scrollView)
        
        scrollView.autoPinEdgesToSuperviewEdges()
        view.layoutIfNeeded()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
