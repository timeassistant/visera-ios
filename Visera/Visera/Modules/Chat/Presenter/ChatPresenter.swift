//
//  ChatPresenter.swift
//  Visera
//
//  Created by Ilia Gutu on 4/20/17.
//  Copyright © 2017 __DevInteractive__. All rights reserved.
//

import UIKit

protocol ChatPresenter {
    
}

class ChatPresenterImp: ChatPresenter {
    
    weak var view: ChatView!
    var interactor: ChatInteractor!
    var router: ChatRouter!
    
    init(view: ChatView, interactor: ChatInteractor, router: ChatRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
   
}
