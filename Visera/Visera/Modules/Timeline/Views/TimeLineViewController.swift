//
//  TimeLineViewController.swift
//  Visera
//
//  Created by Ilia Gutu on 4/20/17.
//  Copyright © 2017 __DevInteractive__. All rights reserved.
//

import UIKit

class TimeLineViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
       // self.view.backgroundColor = UIColor.red
        setup()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func setup(){
        let label = UIView()
        label.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        label.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(label)
        //label.autoPinEdge(toSuperviewEdge: .top, withInset: 20)
        label.autoPinEdgesToSuperviewEdges(with: UIEdgeInsetsMake(20, 20, 20, 20))
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
